console.log("Hello World");

//Array Methods
//JS has built-in functions and methods for arrays. This allows us to manipulate and access array items

//Mutator Methods

/*
	-Mutator Methods are functions that "mutate" or change an array after they're created
	-These methods manipulate the original array performing various tasks such as adding and removing elements
*/

	let fruits = ['Apple','Orange',"Kiwi","Dragon Fruit"];

	//push()
	 /*
		-adds an element in the end of an array AND returns the array's length
		-Syntax
			arrayName.push();
	 */

	 console.log('Current Array: ');
	 console.log(fruits); //log fruits array
	 let fruitsLength = fruits.push('Mango');
	 console.log(fruitsLength);//5
	 console.log('Mutated array from push method:');
	 console.log(fruits);//Mango is added at the end of the array

	 fruits.push('Avocado','Guava');
	 console.log("Mutated array from push method:")
	 console.log(fruits);//

	 //pop()
	 /*
		Removes the last element in an array AND returns the removed element
		-Syntax
			arrayName.pop();
	 */

	 let removedFruit = fruits.pop();
	 console.log(removedFruit);//Guava
	 console.log('Mutated array from pop method:');
	 console.log(fruits);//Guava is removed

	 /*
		Mini Activity
		create a function which will unfriend the last person in the array
		log the ghostFighters array in our console
	 */

	 let ghostFighters = ["Eugene","Dennis","Alfred","Taguro"];

	 function unfriend(){
	 	ghostFighters.pop();
	 };

	 unfriend();
	 console.log(ghostFighters);

	 //unshift()
	 /*
		-adds one or more lements at the beginning of an array
		-Syntax
			arrayName.unshift('elementA');
			arrayName.unshift('elementA',elementB);
	 */

	 fruits.unshift('Lime','Banana');
	 console.log("Mutated array from unshift method:");
	 console.log(fruits);

	 //shift()

	 /*
		removes an element at the beginning of an array AND returns the removed element

		-syntax
			arrayName.shift();
	 */

	 let anotherFruit = fruits.shift();
	 console.log(anotherFruit);//Lime
	 console.log("Mutated array from shift method:");
	 console.log(fruits);//the Lime element is removed from an array


	 //splice
	 /*
		simultaneously removes elements from a specified index number and adds elements
		-Syntax
			arrayName.splice(startingIndex,deleteCount,elementsToBeAdded);
	 */

	 fruits.splice(1,2,'Lime','Cherry');
	 console.log('Mutated array from splice method');
	 console.log(fruits);


	 //sort
	 /*
		Rearranges the array elements in alphanumeric order
		-Syntax
			arrayName.sort();

	 */
	 	fruits.sort();
	 	console.log("Mutated array from sort method:");
	 	console.log(fruits);

	 //reverse
	 /*
		Reverses the order of array elements
		-Syntax
			arrayName.reverse();
	 */
	 fruits.reverse();
	 	console.log("Mutated array from reverse method:");
	 	console.log(fruits);

//Non-Mutator Methods
/*
	Non-Mutator methods are functions that do not modify or change an array after they are created
	-these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
*/

let countries = ['US',"PH",'CAN',"SG","TH","PH",'FR','DE'];

//indexOf()

/*
	returns the index number of the first matching element found in an array
	-if no match is found, the result is -1
	-the search process will be done from first element proceesing to the last element
	-Syntax
		arrayName/indexOf(searchValue);
		arrayName/indexOf(searchValue,fromIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log("Result of indexOf method: " + invalidCountry);

//lastIndexOf()
/*
	Returns the index number of the last matching element found in an array
	-the search process will be done from the last element proceeding to the first element
	Syntax
		arrayName.lastIndexOf(searchvalue);
		arrayName.lastIndexOf(searchvalue, fromIndex);
*/

let lastIndex = countries.lastIndexOf('PH',3);
console.log('Result of lastIndexOf method: ' + lastIndex);

//slice()

/*
	-portions/slices elements from an array AND returns a new array
	-Syntax
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex,endingIndex);
*/

// Slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log('Result from slice method: ');
console.log(slicedArrayA);

// Slicing off elements from a specified index to another index
let slicedArrayB = countries.slice(2,4);
console.log('Result from slice method: ');
console.log(slicedArrayB);

// Slicing off elements starting from the last element of an array
let slicedArrayC = countries.slice(-3);
console.log('Result from slice method: ');
console.log(slicedArrayC);

//toString();
/*
	Returns an array as a string separated by commas
	-Syntax
	arrayName.toString();
*/

let stringArray = countries.toString();
console.log('Result from toString method: ');
console.log(stringArray);

//concat()
/*
	-combines two arrays and returns the combined result
	-Syntax
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let taskArrayA = ['drink html','eat javascript'];
let taskArrayB = ['inhale css','breathe sass'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log('Result from concat method: ');
console.log(tasks);

//Combine multiple arrays

console.log('Result from concat method: ');
let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTasks);

//Combining arrays with elements
let combinedTasks = taskArrayA.concat('smell express','throw react');
console.log("Result from concat method: ");
console.log(combinedTasks);

//join()
/*
	-Returns an array as a string separated by specified separator string
	-Syntax
		arrayName.join('separatorString')
*/

let users = ["John","Jane","Joe","Robert","Nej"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

//Iteration Method

/*
	Iteration methods are loops designed to perform repetitive tasks on arrays
	Iteration methods loops over all items in an array
	Useful for mani[ulating array data resulting in complex tasks
*/

//forEach
/*
	Similar to a for loop that iterates on each array element
	-for each item in the array, the anonymous function passed in the forEach() method will be run
	-The anonymous function is able to recieve the current item being iterated or loop over by assigning a parameter
	-Variable names for arrays are normally writeen in the plural form of the data stored in an array
	-It's common practice to use the singuluar form of the array content for parameter names used in array loops
	-forEach() does NOT return anything
	-Syntax
		arrayName.forEach(function(indivElement){
				statement
		})
*/

	allTasks.forEach(function(task){
		console.log(task);
	});

	/*
		Mini Activity
		create a function that can display the ghostFighters one by one in our console
		Invoke the function
		send a screenshot
	*/

	function displayGhostFighters(){
		ghostFighters.forEach(function(fighter){
			console.log(fighter);
		});
	}
	displayGhostFighters();

	//Using forEach with conditional statements

	//Looping through all array items
	/*
		It is a good practice to print the current elemnt in the console when working with array iteration methods to have an idea of what information is being worked on for each iteration of the loop

		Creatng a sperate variable to store results of an array iteration iteration methods are also good practice to avoid confustion by modifying the original array

		Mastering loops and arrays allow us developers to perform a wide range of features that help in data management and analysis

	*/

	let filteredTasks = [];

	allTasks.forEach(function(task){

		console.log(task);

		if(task.length>10){
			console.log(task);
			filteredTasks.push(task)
		}
	});

	console.log('Result of filtered tasks: ');
	console.log(filteredTasks);

//map()
	/*
		- iterates on each elememnt AND returns new array with different values depending on the result of the function's operation

		-syntax
			let/const resultArray = arrayName.map(function(indivElement))

	*/

	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(number){
		return number * number;
	});

	console.log("Original Array: ");
	console.log(numbers);
	console.log('Result of map method: ')
	console.log(numberMap);

	//map() vs forEach()

	let numberForEach = numbers.forEach(function(number){
		return number * number;
	});

	console.log(numberForEach);//

	//every()
	/*
		-checks if all elements in an array meet the given conditions
		-this is useful for validating data storesd in arrays especially when dealing with large amounts of data
		-returns true if all elements meet teh condition, otherwise false
		-Syntax
		let/const resultArray = arrayName.every(function(indivElement){
			return expression/condition;
		})
	*/

	let allValid = numbers.every(function(number){
		return (number <3);
	});
	console.log("Result of every method: ");
	console.log(allValid);//false

	//some()
	/*
		Checks if at least one element in the array meets the given condition
		-returns a true value if at least one element meets the condition and false if otherwise
		Syntax
			let/const resultArray = arrayName.some(function(indivElement){
			return expression/condition;
			})
	*/

	let someValid = numbers.some(function(number){
		return (number<2);
	});
	console.log("Result of some method: ");
	console.log(someValid);//true

	//Combining the returned result from every/some method may be used in other statements to perform consecutive results
	if(someValid){
		console.log('Some numbers in the array are greater than 2');
	}

	//filter
	/*
		returns a new array that contains elements which meets a given condition
		-returns an empty array if no elements were found
		syntax:

		let/const resultArray = arrayName.filter(function(indivElemet){
				return expression/condition;
		})

	*/

	let filterValid = numbers.filter(function(number){
		return (number<3);
	})
	console.log("Result of filter method: ");
	console.log(filterValid);//[1,2];

	let nothingFound = numbers.filter(function(number){
		return (number = 0);
	})
	console.log("Result of filter method");
	console.log(nothingFound);

	//Filtering using forEach

	let filteredNumbers = [];

	numbers.forEach(function(number){
		// console.log(number);

		if(number<3){
			filteredNumbers.push(number);
		}
	});
	console.log("Result of filter method: ");
	console.log(filteredNumbers);

//includes()

/*
	checks if the argument passed can be found in the array
		-it returns a boolean which can be saved in a variable
			-returns true if the argument is found the array
			-returns false if it is not

		Syntax:
		arrayName.includes(<argumentToFind>)

*/


	let products = ["Mouse","Keyboard","Laptop","Monitor"];

	let productFound = products.includes("Mouse");
	console.log(productFound);//true

	let productNotFound = products.includes("Headset");
	console.log(productNotFound);//false


	//Method Chaining
		//Methods can be chained using them one after another
		//The result of the first method is used on the second method until all "chained" methods have been resolved

		let filteredProducts = products.filter(function(product){
			return product.toLowerCase().includes('a');
		});

		console.log(filteredProducts);

		/*
			Mini Activity
			Create an addTrainer function that will enable us to add a trainer in the contacts array
			--This function should be able to receive a string
			--Determine if the added trainer already exists in the contacts array:
			--if it is, show an alert saying "Already added in the Match Call"
			--if it is not, add the trainer in the contacts array and show an alert saying "Registered!"
			--invoke and add a trainer in the browser's console
			--in the console, log the contacts array
		*/

		let contacts = ["Ash"];

		function addTrainer(trainer){

			let doesTrainerExist = contacts.includes(trainer);

			if(doesTrainerExist){
				alert("Already added in the Match Call.");
			}else{
				contacts.push(trainer);
				alert("Registered!");
			}

		}

		//reduce
		/*
			-evaluates elements from left to right and returns/reduces the array into a single value
			-Syntax
			let/const resultArray = arrayName.reduce(function(accumulator,currenValue){
				return expression/operation
			})
				-"accumulator" parameter in the function stores the result for every iteration of the loop
				="currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
			
			How the "reduce" method works
			1. the first/result element in the array is stored in the "accumulator" parameter
			2/ the second/next element in the array is stored in the currentvalue parameter
			3. an operation is performed ont he two elemts
			4. the loop repates step 1-3 until all elements have been worked on


		*/

		console.log(numbers);
		let iteration = 0;
		let iterationStr = 0;

		let reducedArray = numbers.reduce(function(x,y){
			console.warn('current iteration: ' + ++iteration);
			console.log('accumulator: ' + x);
			console.log('current value: ' + y);

			return x + y;
		})

		console.log("Result of reduce method: " + reducedArray);//15

		let list = ["Hello", "Again", "World"];

		let reducedJoin = list.reduce(function(x,y){
			console.warn('current iteration: ' + ++iterationStr);
			console.log('accumulator: ' + x);
			console.log('current value: ' + y);

			return x + ' ' + y;
		})

		console.log("Result of reduce method: " + reducedJoin);